﻿using Core.Framework.EntityExtend.AiExpression;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace Core.Framework.EntityExtend.Model
{
    public class EFCoreCommon
    {

        public static object GetValueByField<T>(T that, string name)
        {
            var field = typeof(T).GetField(name, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);

            return field.GetValue(that);
        }

        /// <summary>
        /// 获取条件
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="table"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        public static Tuple<Mapping<T>, string> GetWhereStr<T>(DbContext context, Expression<Func<T, bool>> where)
            where T : class, new()
        {
            // 字段解析
            var mapping = EFCoreModel<T>.GetModelMapping(context);

            // 生成条件
            return new Tuple<Mapping<T>, string>(
                mapping,
                new AiExpConditions<T>(where, mapping.rows).GetWhere());
        }


        /// <summary>
        /// 实体转列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="rows"></param>
        /// <returns></returns>
        public static DataTable ListToDataTable<T>(List<T> list, string tableName, Dictionary<string, FiledParameter> rows)
            where T : class, new()
        {
            if (list == null || list.Count <= 0)
                return null;

            DataTable table = new DataTable(tableName);

            bool createColumn = true;

            foreach (T model in list)
            {
                if (model == null)
                    continue;

                var row = table.NewRow();

                foreach (var item in typeof(T).GetProperties())
                {

                    var filed = rows[item.Name].name;

                    if (createColumn)
                        table.Columns.Add(new DataColumn(filed, Nullable.GetUnderlyingType(
            item.PropertyType) ?? item.PropertyType));

                    row[filed] = item.GetValue(model, null) ?? DBNull.Value;
                }

                if (createColumn)
                    createColumn = false;

                table.Rows.Add(row);
            }

            return table;

        }

    }
}
