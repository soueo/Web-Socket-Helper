﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;

namespace Core.Framework.EntityExtend.Model
{
    public class EFCoreTaskParameter
    {
        #region Parameter

        /// <summary>
        /// 当前需处理上文
        /// </summary>
        public static ConcurrentDictionary<DbContext, List<string>>
            dictionary = new ConcurrentDictionary<DbContext, List<string>>();

        /// <summary>
        /// 批量插入
        /// </summary>
        public static ConcurrentDictionary<DbContext, List<DataTable>>
            BulkTable = new ConcurrentDictionary<DbContext, List<DataTable>>();

        #endregion
    }
}
