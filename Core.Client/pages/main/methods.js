var LoadAppList = function(that, Request_1) {


	new Request_1.Request('https://app.help-itool.com/ApiExtend/home')
		.Then(e => {
			
			e.historys.push({
				logo: 'https://img-cdn-qiniu.dcloud.net.cn/uploads/avatar/000/41/02/56_avatar_mid.jpg',
				title: '小码博客',
				url: 'http://www.janjanle.com/'
			});

			e.historys.push({
				logo: 'https://www.help-itool.com/page/shopdemo/static/temp/banner3.jpg',
				title: '电商模板',
				url: 'https://www.help-itool.com/Page/shopdemo/index.html'
			});

			that.applist = e.historys;
		});

}

export default {

	PageInit: function(that, Request_1,helper) {
		
		helper.GetWevViewJSPath();

		LoadAppList(that, Request_1);

		that.ClientRect = uni.createSelectorQuery().select("#message-list").boundingClientRect(data => {
			if (that.demo.openmenuBar) {
				if (data.top < 1) {
					that.demo.openmenuBar = false;
					that.overflow = 'inherit';
					that.minHeight = data.height + 'px';
					uni.pageScrollTo({
						scrollTop: 80,
						duration: 100
					});
				} else if (data.top == 80) {} else if (data.top > 70) {
					uni.pageScrollTo({
						scrollTop: 0,
						duration: 200
					});
				} else {
					that.demo.openmenuBar = false;
					that.overflow = 'inherit';
					that.minHeight = data.height + 'px';
					uni.pageScrollTo({
						scrollTop: 80,
						duration: 100
					});
				}
			} else {
				if (data.top > 10) {
					that.demo.openmenuBar = true;
					that.overflow = 'hidden';
					that.minHeight = that.winHeight;
					uni.pageScrollTo({
						scrollTop: 0,
						duration: 200
					});
				}
			}
		});

		uni.getSystemInfo({
			success: function(e) {
				setTimeout(function() {
					that.winHeight = (e.windowHeight - 5) + 'px';
					that.minHeight = that.winHeight;
					setTimeout(function() {
						uni.pageScrollTo({
							scrollTop: 80,
							duration: 300
						});
					}, 500);
				}, 1000);
			}
		});

	},

	onNavigationBarButtonTap: function(that, e, helper, Config, Request_1, AppChat) {
		switch (e.index) {
			case 0:
				uni.scanCode({
					success: function(res) {

						console.log(res.result)

						var data = JSON.parse(res.result);
						switch (data.type) {
							case 'adduser':
								var item = data.data;
								new Request_1.Request(Config.GlobalSocketUser.AddMember, {
										UserId: Config.UserInfo().id,
										parameter: JSON.stringify({
											userkey: item.id,
											userName: item.info.userName,
											headImg: item.info.headImg,
											ori: '二维码分享'
										}),
										title: '',
										ProjectToken: Config.UserInfo().projectToken
									})
									.Then(a => {
										helper.SetRedDot(true, 1);

										uni.showToast({
											title: '添加好友成功，请查看通讯录'
										});
									})
									.Post();
								break;
							case 'qrlogin':

								AppChat.TaskLogin(that, {
									content: 'open',
									sesstion: data.sesstion
								});

								uni.showModal({
									title: '用户提醒',
									content: '点击确认登陆信息？',
									success: function(res) {

										if (res.confirm) {

											var userInfo = Global.UserInfo();

											AppChat.TaskLogin(that, {
												content: JSON.stringify(userInfo),
												sesstion: data.sesstion
											});

										} else if (res.cancel) {

											AppChat.TaskLogin(that, {
												content: 'close',
												sesstion: data.sesstion
											});

										}
									}
								});

								break;

							case 'addgroup':
								var item = data.data;
								var userInfo = Config.UserInfo();
								new Request_1.Request(Config.GlobalSocketUser.AddGroup, {
										UserId: userInfo.id,
										Level: 3,
										GroupKey: item.id,
										Paras: JSON.stringify({
											userName: userInfo.paras.userName,
											headImg: userInfo.paras.headImg,
											ori: '二维码分享'
										}),
										ProjectToken: Config.UserInfo().projectToken
									})
									.Then(a => {
										helper.SetRedDot(true, 1);

										console.log(JSON.stringify(a))

										uni.showToast({
											title: '添加群组成功，请查看通讯录'
										});
									})
									.Post();
								break;

							default:
								break;
						}
					},
					fail(e) {
						console.log(123);

						console.log(JSON.stringify(e));
					}
				});
				break;
			case 1:
				helper.SetRedDot(false, 1);
				uni.navigateTo({
					url: '../tree/tree'
				});
				break;
			default:
				break;
		}

	},

	ToWebView: function(item,helper) {
		
		
		var webviewjs = helper.GetWevViewJSPath();
		
		if(!webviewjs){
			console.error('webview.js loading error')
			return;
		}



		var appKey = item.url.split('?')[0];


		var appMain = plus.webview.getWebviewById('appkey');


		if (appMain) {
			appMain.show('slide-in-bottom');
		} else {

			var webview = plus.webview.create(item.url, 'appkey', {
				blockNetworkImage: true
			},{
				filepath:webviewjs,
				appkey:'appkey',
				appmain:'appkey'
			});
			
			console.log(webviewjs)
			
			webview.setJsFile(webviewjs);

			// webview.overrideUrlLoading({
			// 	mode: 'reject'
			// }, function(e) {
			// 	console.log('reject url: ' + e.url);
			// });

			webview.show('slide-in-bottom');

		}

	}
};
