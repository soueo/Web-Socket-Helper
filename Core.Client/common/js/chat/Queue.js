import Request_1 from './Request.js'
import Global_1 from './config.js'

/** 消息处理 */
var Queue = /** @class */ (function() {
	function Queue() {}
	Queue.prototype.Push = function(model, callback, error) {
		if (callback === void 0) {
			callback = null;
		}
		if (error === void 0) {
			error = null;
		}
		if (!model.ClientToken) {
			console.error("model ClientToken is null");
			return;
		}
		if (!model.Template) {
			console.error("model Template is null");
			return;
		}
		if (!model.MessageKey) {
			console.error("model MessageKey is null");
			return;
		}
		if (!model.Content) {
			console.error("model Content is null");
			return;
		}
		var request = new Request_1.Request(Global_1.GlobalQueue.QueuePlus.toString(), model);
		request.Then(function(e) {
			callback && callback(e);
		}).Catch(function(e) {
			error(e);
		}).Post();
	};
	return Queue;
}());

export default {
	Queue: Queue
};
