﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Framework.Model.WebSockets
{
    public class Message
    {
        /// <summary>
        /// 消息标识
        /// </summary>
        public string MessageKey { get; set; }

        /// <summary>
        /// 消息[属性]参数
        /// </summary>
        public string Parameter { get; set; }

        /// <summary>
        /// 消息类型
        /// </summary>
        public MessageTypeEnum MessageType { get; set; }

        /// <summary>
        /// 消息内容
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 发送时间
        /// </summary>
        public DateTime SendDateTime { get; set; } = DateTime.Now;


    }
}
