﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Framework.Model.WebSockets
{
    /// <summary>
    /// 队列消息
    /// </summary>
    public class MessageQueue
    {
        /// <summary>
        /// 消息主题
        /// </summary>
        public string Template { get; set; }

        /// <summary>
        /// 重试次数
        /// </summary>
        public int RetryCount { get; set; } = 0;

        /// <summary>
        /// 发送方身份信息
        /// </summary>
        public ClientInfo ClientInfo { get; set; }

        /// <summary>
        /// 消息体
        /// </summary>
        public Message Message { get; set; }
    }
}
