﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Framework.Model.ViewModel
{
    public class ScoketUserRequest
    {
        public int key { get; set; }
        public int hour { get; set; } = 24 * 3;
        public string user { get; set; }
        public string uuid { get; set; }
        public string phone { get; set; }
        public string wchat { get; set; }
        public string token { get; set; }
        public string password { get; set; }
        public string projectToken { get; set; }
        public string parameter { get; set; }
        public string url { get; set; }
    }
}
