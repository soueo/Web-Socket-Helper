﻿using Core.WebSocketApi.BaseAttribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.WebSocketApi
{
    public enum CodeResult
    {
        /// <summary>
        /// 成功
        /// </summary>
        [Remark("成功")]
        SUCCESS = 200,

        #region 参数错误
        /// <summary>
        /// 参数为空
        /// </summary>
        [Remark("参数为空")]
        PARAMS_IS_NULL = 10001,
        /// <summary>
        /// 参数不全
        /// </summary>
        [Remark("参数不全")]
        PARAMS_NOT_COMPLETE = 10002,
        /// <summary>
        /// 参数类型错误
        /// </summary>
        [Remark("参数类型错误")]
        PARAMS_TYPE_ERROR = 10003,
        /// <summary>
        /// 参数无效
        /// </summary>
        [Remark("参数无效")]
        PARAMS_IS_INVALID = 10004,
        #endregion

        #region 用户错误
        /// <summary>
        /// 用户不存在
        /// </summary>
        [Remark("用户不存在")]
        USER_NOT_EXIST = 20001,
        /// <summary>
        /// 用户未登陆
        /// </summary>
        [Remark("用户未登陆")]
        USER_NOT_LOGGED_IN = 20002,
        /// <summary>
        /// 用户名或密码错误
        /// </summary>
        [Remark("用户名或密码错误")]
        USER_ACCOUNT_ERROR = 20003,
        /// <summary>
        /// 用户账户已被禁用
        /// </summary>
        [Remark("用户账户已被禁用")]
        USER_ACCOUNT_FORBIDDEN = 20004,
        /// <summary>
        /// 用户已存在
        /// </summary>
        [Remark("用户已存在")]
        USER_HAS_EXIST = 20005,
        #endregion

        #region 业务错误
        /// <summary>
        /// 系统业务出现问题
        /// </summary>
        [Remark("系统业务出现问题")]
        BUSINESS_ERROR = 30001,
        #endregion

        #region 系统错误
        /// <summary>
        /// 系统内部错误
        /// </summary>
        [Remark("系统内部错误")]
        SYSTEM_INNER_ERROR = 40001,
        #endregion

        #region 数据错误
        /// <summary>
        /// 数据未找到
        /// </summary>
        [Remark("数据未找到")]
        DATA_NOT_FOUND = 50001,
        /// <summary>
        /// 数据有误
        /// </summary>
        [Remark("数据有误")]
        DATA_IS_WRONG = 50002,
        /// <summary>
        /// 数据已存在
        /// </summary>
        [Remark("数据已存在")]
        DATA_ALREADY_EXISTED = 50003,
        #endregion

        #region 接口错误
        /// <summary>
        /// 系统内部接口调用异常
        /// </summary>
        [Remark("系统内部接口调用异常")]
        INTERFACE_INNER_INVOKE_ERROR = 60001,
        /// <summary>
        /// 系统外部接口调用异常
        /// </summary>
        [Remark("系统外部接口调用异常")]
        INTERFACE_OUTER_INVOKE_ERROR = 60002,
        /// <summary>
        /// 接口禁止访问
        /// </summary>
        [Remark("接口禁止访问")]
        INTERFACE_FORBIDDEN = 60003,
        /// <summary>
        /// 接口地址无效
        /// </summary>
        [Remark("接口地址无效")]
        INTERFACE_ADDRESS_INVALID = 60004,
        /// <summary>
        /// 接口请求超时
        /// </summary>
        [Remark("接口请求超时")]
        INTERFACE_REQUEST_TIMEOUT = 60005,
        /// <summary>
        /// 接口负载过高
        /// </summary>
        [Remark("接口负载过高")]
        INTERFACE_EXCEED_LOAD = 60006,
        #endregion

        #region 权限错误
        /// <summary>
        /// 没有访问权限
        /// </summary>
        [Remark("没有访问权限")]
        PERMISSION_NO_ACCESS = 70001
        #endregion

    }
}
