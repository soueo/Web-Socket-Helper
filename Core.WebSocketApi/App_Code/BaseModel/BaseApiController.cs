﻿using Core.DataAccess.Model.Project.Queue;
using Core.DataAccess.Model.Projects;
using Core.Framework.Model.Common;
using Core.IBusiness.IProjectModule;
using Core.IBusiness.ISocketModel;
using Core.IBusiness.IUserModule;
using Microsoft.AspNetCore.Mvc;

namespace Core.WebSocketApi
{
    public class BaseApiController : ControllerBase
    {
        private IProject _iProject;
        private IProjectUser _iProjectUser;
        private ISocketGroup _iSocketGroup;
        private ISocketRelationship _iSocketRelationship;
        private ISocketMessage _iSocketMessage;
        private ISocketUser _iSocketUser;
        private ProjectContext _iProjectContext;
        private ProjectSocketContext _iProjectSocketContext;


        /// <summary>
        /// 项目信息|操作
        /// </summary>
        protected IProject iProject
        {
            get {
                if (this._iProject == null)
                    this._iProject = BuildServiceProvider.GetService<IProject>();
                return _iProject;
            }
        }

        /// <summary>
        /// 用户操作
        /// </summary>
        protected IProjectUser iProjectUser
        {
            get {
                if (this._iProjectUser == null)
                    this._iProjectUser = BuildServiceProvider.GetService<IProjectUser>();
                return _iProjectUser;
            }
        }

        /// <summary>
        /// 群组管理
        /// </summary>
        protected ISocketGroup iSocketGroup
        {
            get {
                if (this._iSocketGroup == null)
                    this._iSocketGroup = BuildServiceProvider.GetService<ISocketGroup>();
                return _iSocketGroup;
            }
        }

        /// <summary>
        /// 用户关系管理
        /// </summary>
        protected ISocketRelationship iSocketRelationship
        {
            get {
                if (this._iSocketRelationship == null)
                    this._iSocketRelationship = BuildServiceProvider.GetService<ISocketRelationship>();
                return _iSocketRelationship;
            }
        }

        /// <summary>
        /// Queue消息管理
        /// </summary>
        protected ISocketMessage iSocketMessage
        {
            get {
                if (this._iSocketMessage == null)
                    this._iSocketMessage = BuildServiceProvider.GetService<ISocketMessage>();
                return _iSocketMessage;
            }

        }

        /// <summary>
        /// Queue 用户操作
        /// </summary>
        protected ISocketUser iSocketUser
        {
            get {
                if (this._iSocketUser == null)
                    this._iSocketUser = BuildServiceProvider.GetService<ISocketUser>();
                return _iSocketUser;
            }
        }

        /// <summary>
        /// 项目管理数据库
        /// </summary>
        protected ProjectContext iProjectContext
        {
            get {
                if (this._iProjectContext == null)
                    this._iProjectContext = BuildServiceProvider.GetService<ProjectContext>();
                return _iProjectContext;
            }
        }

        /// <summary>
        /// 消息管理数据库
        /// </summary>
        protected ProjectSocketContext iProjectSocketContext
        {
            get {
                if (this._iProjectSocketContext == null)
                    this._iProjectSocketContext = BuildServiceProvider.GetService<ProjectSocketContext>();
                return _iProjectSocketContext;
            }
        }
    }
}
