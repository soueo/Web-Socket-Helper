﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.WebSocketApi.BaseAttribute
{
    public class RemarkAttribute : Attribute
    {
        /// <summary>
        /// 备注信息
        /// </summary>
        public string Info { get; set; }

        public RemarkAttribute(string info) { this.Info = info; }
    }
}
