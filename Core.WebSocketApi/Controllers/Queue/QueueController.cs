﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Framework.Model.WebSockets;
using Core.Framework.Redis;
using Core.Framework.Redis.Queue_Helper;
using Core.Framework.Util;
using Core.Middleware.WebSockets;
using Core.WebSocketApi.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Core.WebSocketApi.Controllers.Queue
{
    [Route(".v1/queue/")]
    [ApiController]
    public class QueueController : BaseApiController
    {
        [HttpPost(".push")]
        public IResult Post(QueryMessage model)
        {

            // 是否发布成功
            bool isPublish = new QueueMsg().DoDispatch(model);

            if (isPublish)
                return new ApiResult { code = CodeResult.SUCCESS };
            else
                return new ApiResult { code = CodeResult.USER_NOT_LOGGED_IN };

        }
    }
}
