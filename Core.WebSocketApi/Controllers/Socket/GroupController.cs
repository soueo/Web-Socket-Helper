﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Core.DataAccess.Model.Project.Queue;
using Core.Framework.Model.ViewModel;
using Core.WebSocketApi.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Core.WebSocketApi.Controllers.Socket
{
    [Route(".v1/socket/[controller]")]
    [ApiController]
    public class GroupController : BaseApiController
    {
        [HttpGet("{id}/{projectToken}")]
        public IResult Get(int id, string projectToken)
        {
            var result = this.iSocketGroup.GetById(id, projectToken);

            return new ApiResult
            {
                code = result.Item2 ? CodeResult.SUCCESS : CodeResult.DATA_NOT_FOUND,
                date = result.Item1
            };
        }

        [HttpGet(".grouplist{userKey}/{projectToken}")]
        [Description("查询用户群组信息列表")]
        public IResult Get(string userKey, string projectToken)
        {
            var result = this.iSocketGroup.GetByUserKey(userKey, projectToken);

            return new ApiResult
            {
                code = result.Item2 ? CodeResult.SUCCESS : CodeResult.DATA_NOT_FOUND,
                date = result.Item1
            };
        }

        [HttpGet(".groups{userKey}/{projectToken}")]
        [Description("查询用户以加入群组集")]
        public IResult GetGroupsByUserKey(string userKey, string projectToken)
        {
            var result = this.iSocketGroup.GetGroupsByUserKey(userKey, projectToken);

            return new ApiResult
            {
                code = result.Item2 ? CodeResult.SUCCESS : CodeResult.DATA_NOT_FOUND,
                date = result.Item1
            };
        }

        [HttpGet(".groupuser{id}/{projectToken}")]
        [Description("查询群组成员")]
        public IResult GetGroupUsersByGroupKey(int id, string projectToken)
        {
            var result = this.iSocketGroup.GetGroupUsersByGroupKey(id, projectToken);

            return new ApiResult
            {
                code = result.Item2 ? CodeResult.SUCCESS : CodeResult.DATA_NOT_FOUND,
                date = result.Item1
            };
        }


        [HttpPost]
        public IResult Create(ProjectModuleGroup model)
        {
            var result = this.iSocketGroup.Create(model);

            return new ApiResult
            {
                code = result.Item3 ? CodeResult.SUCCESS : CodeResult.PARAMS_IS_INVALID,
                date = result.Item1,
                info = result.Item2
            };
        }

        [HttpPost(".add")]
        [Description("加入群组")]
        public IResult AddGroupUser(ProjectModuleGroupUser model)
        {
            var result = this.iSocketGroup.AddGroupUser(model);

            return new ApiResult
            {
                code = result.Item3 ? CodeResult.SUCCESS : CodeResult.PARAMS_IS_INVALID,
                date = result.Item1,
                info = result.Item2
            };
        }
        

        [HttpPut]
        public IResult Update(ProjectModuleGroup model)
        {
            var result = this.iSocketGroup.Update(model);

            return new ApiResult
            {
                code = result.Item3 ? CodeResult.SUCCESS : CodeResult.PARAMS_IS_INVALID,
                date = result.Item1,
                info = result.Item2
            };
        }

        [HttpPut(".update")]
        [Description("修改组员信息")]
        public IResult UpdateGroupUser(ProjectModuleGroupUser model)
        {
            var result = this.iSocketGroup.UpdateGroupUser(model);

            return new ApiResult
            {
                code = result.Item3 ? CodeResult.SUCCESS : CodeResult.PARAMS_IS_INVALID,
                date = result.Item1,
                info = result.Item2
            };
        }


        [HttpDelete("{id}")]
        [Description("解散群组")]
        public IResult Delete(ScoketGroupRequest request)
        {
            var result = this.iSocketGroup.Delete(request.id, request.userKey, request.projectToken);

            return new ApiResult
            {
                code = result.Item2 ? CodeResult.SUCCESS : CodeResult.PARAMS_IS_INVALID,
                date = result.Item1
            };
        }
    }
}
