﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Framework.Model.Common;
using Core.Framework.Model.ViewModel;
using Core.WebSocketApi.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Core.WebSocketApi.Controllers.Socket
{
    [Route(".v1/socket/[controller]")]
    [ApiController]
    public class MessageController : BaseApiController
    {
        [HttpPost]
        public IResult GetOfflineMessages(ScoketMessageRequest model)
        {
            var result = this.iSocketMessage.GetOfflineMessages(model.projectToken, model.pagination);
            return new ApiResult
            {
                code = result == null ? CodeResult.DATA_NOT_FOUND:CodeResult.SUCCESS,
                date = result
            };
        }

        [HttpPost(".key")]
        public IResult GetOfflineMessagesByMsgKey(ScoketMessageRequest model)
        {
            var result = this.iSocketMessage.GetOfflineMessagesByMsgKey(model.msgKey, model.projectToken, model.pagination);
            return new ApiResult
            {
                code = result == null ? CodeResult.DATA_NOT_FOUND : CodeResult.SUCCESS,
                date = result
            };
        }

        [HttpPost(".keyAsTemp")]
        public IResult GetOfflineMessagesByMsgKeyAsTemplate(ScoketMessageRequest model)
        {
            var result = this.iSocketMessage
                .GetOfflineMessagesByMsgKeyAndTemplate(model.msgKeys, model.template, model.projectToken, model.pagination);
            return new ApiResult
            {
                code = result == null ? CodeResult.DATA_NOT_FOUND : CodeResult.SUCCESS,
                date = result
            };
        }


        [HttpPost(".groupMsg")]
        public IResult GetGroupMessages(ScoketMessageRequest model)
        {
            var result = this.iSocketMessage.GetGroupMessages(model.projectToken, model.pagination);
            return new ApiResult
            {
                code = result == null ? CodeResult.DATA_NOT_FOUND : CodeResult.SUCCESS,
                date = result
            };
        }

        [HttpPost(".groupMsgBykey")]
        public IResult GetGroupMessagesByMsgKey(ScoketMessageRequest model)
        {
            var result = this.iSocketMessage.GetGroupMessagesByMsgKey(model.msgKey, model.projectToken, model.pagination);
            return new ApiResult
            {
                code = result == null ? CodeResult.DATA_NOT_FOUND : CodeResult.SUCCESS,
                date = result
            };
        }

        [HttpPost(".groupMsgByKeyAsTemp")]
        public IResult GetGroupMessagesByMsgKeyAsTemplate(ScoketMessageRequest model)
        {
            var result = this.iSocketMessage.GetGroupMessagesByMsgKeyAndTemplate(model.msgKey, model.template, model.projectToken, model.pagination);
            return new ApiResult
            {
                code = result == null ? CodeResult.DATA_NOT_FOUND : CodeResult.SUCCESS,
                date = result
            };
        }


        [HttpPost(".backupsMsg")]
        public IResult GetBackupsMessages(ScoketMessageRequest model)
        {
            var result = this.iSocketMessage.GetBackupsMessages(model.projectToken, model.pagination);
            return new ApiResult
            {
                code = result == null ? CodeResult.DATA_NOT_FOUND : CodeResult.SUCCESS,
                date = result
            };
        }

        [HttpPost(".backupsMsgBykey")]
        public IResult GetBackupsMessagesByMsgKey(ScoketMessageRequest model)
        {
            var result = this.iSocketMessage.GetBackupsMessagesByMsgKey(model.msgKey, model.projectToken, model.pagination);
            return new ApiResult
            {
                code = result == null ? CodeResult.DATA_NOT_FOUND : CodeResult.SUCCESS,
                date = result
            };
        }

        [HttpPost(".backupsMsgBykeyAsTemp")]
        public IResult GetBackupsMessagesByMsgKeyAsTemplate(ScoketMessageRequest model)
        {
            var result = this.iSocketMessage.GetBackupsMessagesByMsgKeyAndTemplate(model.msgKey, model.template, model.projectToken, model.pagination);
            return new ApiResult
            {
                code = result == null ? CodeResult.DATA_NOT_FOUND : CodeResult.SUCCESS,
                date = result
            };
        }

    }
}
