﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Core.DataAccess.Model.Project.Queue;
using Core.WebSocketApi.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Core.WebSocketApi.Controllers.Socket
{
    [Route(".v1/socket/[controller]")]
    [ApiController]
    public class RelationshipController : BaseApiController
    {
        [HttpGet("{userKey}/{projectToken}")]
        [Description("查询用户关系 [分组列表]|[好友列表]")]
        public IResult GetAll(string userKey, string projectToken)
        {
            var result = this.iSocketRelationship.GetAll(userKey, projectToken);
            return new ApiResult
            {
                code = result.Item3 ? CodeResult.SUCCESS : CodeResult.DATA_NOT_FOUND,
                date = new
                {
                    ProjectUserGroup = result.Item1,
                    ProjectUserGroupMember = result.Item2
                }
            };
        }

        [HttpGet(".groupKey{groupKey}/{projectToken}")]
        [Description("查询指定得分组好友")]
        public IResult GetGroupMembersByGroupKey(int groupKey, string projectToken)
        {
            var result = this.iSocketRelationship.GetGroupMembersByGroupKey(groupKey, projectToken);
            return new ApiResult
            {
                code = result.Item2 ? CodeResult.SUCCESS : CodeResult.DATA_NOT_FOUND,
                date = result.Item1
            };
        }

        [HttpGet(".groupMember{id}/{projectToken}")]
        [Description("查询指定好友信息")]
        public IResult GetItemGroupMember(int id, string projectToken)
        {
            var result = this.iSocketRelationship.GetItemGroupMember(id, projectToken);
            return new ApiResult
            {
                code = result.Item2 ? CodeResult.SUCCESS : CodeResult.DATA_NOT_FOUND,
                date = result.Item1
            };
        }


        [HttpPost(".group")]
        [Description("创建分组")]
        public IResult CreateGroup(ProjectUserGroup model)
        {
            var result = this.iSocketRelationship.CreateGroup(model);
            return new ApiResult
            {
                code = result.Item3 ? CodeResult.SUCCESS : CodeResult.DATA_NOT_FOUND,
                date = result.Item1,
                info = result.Item2
            };
        }

        [HttpPost(".member")]
        [Description("创建成员")]
        public IResult CreateGroupMember(ProjectUserGroupMember model)
        {
            var result = this.iSocketRelationship.CreateGroupMember(model);
            return new ApiResult
            {
                code = result.Item3 ? CodeResult.SUCCESS : CodeResult.DATA_NOT_FOUND,
                date = result.Item1,
                info = result.Item2
            };
        }


        [HttpPut]
        [Description("修改分组")]
        public IResult UpdateGroup(ProjectUserGroup model)
        {
            var result = this.iSocketRelationship.UpdateGroup(model);
            return new ApiResult
            {
                code = result.Item3 ? CodeResult.SUCCESS : CodeResult.DATA_NOT_FOUND,
                date = result.Item1,
                info = result.Item2
            };
        }

        [HttpPut(".groupMember")]
        [Description("修改分组成员")]
        public IResult UpdateGroupMember(ProjectUserGroupMember model)
        {
            var result = this.iSocketRelationship.UpdateGroupMember(model);
            return new ApiResult
            {
                code = result.Item3 ? CodeResult.SUCCESS : CodeResult.DATA_NOT_FOUND,
                date = result.Item1,
                info = result.Item2
            };
        }


        [HttpDelete("{id}/{projectToken}")]
        public IResult DeleteGroup(int id, string projectToken)
        {
            var result = this.iSocketRelationship.DeleteGroup(id, projectToken);
            return new ApiResult
            {
                code = result.Item2 ? CodeResult.SUCCESS : CodeResult.DATA_NOT_FOUND,
                date = result.Item1
            };
        }

        [HttpDelete(".member{id}/{projectToken}")]
        public IResult DeleteGroupMember(int id, string projectToken)
        {
            var result = this.iSocketRelationship.DeleteGroupMember(id, projectToken);
            return new ApiResult
            {
                code = result.Item2 ? CodeResult.SUCCESS : CodeResult.DATA_NOT_FOUND,
                date = result.Item1
            };
        }
    }
}
