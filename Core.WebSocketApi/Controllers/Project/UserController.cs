﻿using Core.DataAccess.Model.Projects;
using Core.Framework.Util;
using Core.WebSocketApi.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel;

namespace Core.WebSocketApi.Controllers.Project
{
    [Route(".v1/project/[controller]")]
    [ApiController]
    public class UserController : BaseApiController
    {
        [HttpGet]
        public IResult Get()
        {
            return new ApiResult
            {
                code  = CodeResult.INTERFACE_ADDRESS_INVALID
            };
        }

        [HttpGet("{token}")]
        [Description("根据Token获取用户信息")]
        public IResult Get(string token)
        {
            var result = this.iProjectUser.GetUserInfoByToken(token);

            return new ApiResult
            {
                code = result.Item1 ? CodeResult.SUCCESS : CodeResult.DATA_NOT_FOUND,
                date = result.Item2
            };
        }

        [HttpPost("{type}")]
        [Description("login | bytoken")]
        public IResult Login(ProjectUser model,string type)
        {
            Tuple<ProjectUser, bool> result;

            var aa = Md5Helper.Hash(model.Pass);

            if (type.ToLower().Equals(".login"))
                result = this.iProjectUser.Login(model.Phone, model.Pass, 24 * 3);

            else if (type.ToLower().Equals(".loginByToken".ToLower()))
                result = this.iProjectUser.LoginByToken(model.Token, model.Pass, 24 * 3);

            else
                return Get();

            return new ApiResult
            {
                code = result.Item2 ? CodeResult.SUCCESS : CodeResult.USER_NOT_EXIST,
                date = result.Item1
            };
        }

        [HttpPost(".{type}.{hour}h")]
        [Description("login | bytoken")]
        public IResult Login(ProjectUser model, string type, int hour)
        {
            Tuple<ProjectUser, bool> result;

            if (type.ToLower().Equals("login"))
                result = this.iProjectUser.Login(model.Phone, model.Pass, hour);

            else if (type.ToLower().Equals("loginByToken".ToLower()))
                result = this.iProjectUser.LoginByToken(model.Token, model.Pass, hour);

            else
                return Get();

            return new ApiResult
            {
                code = result.Item2 ? CodeResult.SUCCESS : CodeResult.USER_NOT_EXIST,
                date = result.Item1
            };
        }

        [HttpPost]
        [Description("reg")]
        public IResult Reg(ProjectUser model)
        {
            var result = this.iProjectUser.Reg(model);

            return new ApiResult
            {
                code = result.Item1 ? CodeResult.SUCCESS : CodeResult.PARAMS_IS_INVALID,
                date = result.Item2,
                info = result.Item3
            };
        }

        [HttpPut("{id}")]
        [Description("update")]
        public IResult UpdateUserInfo(ProjectUser model)
        {
            var result = this.iProjectUser.UpdateUserInfo(model);
            return new ApiResult
            {
                code = result != null ? CodeResult.SUCCESS : CodeResult.PARAMS_NOT_COMPLETE,
                date = result
            };
        }

        [HttpDelete("{token}")]
        [Description("outLogin")]
        public void OutLogin(string token)
        {
            this.iProjectUser.OutLogin(token);
        }

    }
}
