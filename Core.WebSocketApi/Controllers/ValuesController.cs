﻿using Core.Framework.Model.Common;
using Core.Framework.Model.WebSockets;
using Core.Framework.Redis;
using Core.Framework.Redis.Queue_Helper;
using Core.Middleware.WebSockets;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace Main.Controllers
{
    [Route("/")]
    [Route("api")]
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        [HttpGet]
        public ActionResult<object> Get()
        {
            var log = BuildServiceProvider.GetService<ILogger<ValuesController>>();

            log.LogDebug("这是一个 debug 日志");

            return WebSocketApplication.ClientsPool;
        }

        [HttpGet(".msg")]
        public ActionResult<object> Msg()
        {
            var redis = new RedisHelper(4);
            return redis.SortedGetRangeByRank<MessageQueue>(RedisQueueHelperParameter.Queue);
        }

        [HttpGet(".sub")]
        public ActionResult<object> sub()
        {
            var redis = new RedisHelper();
            return redis.HashGet<List<Listener_Template>>(RedisQueueHelperParameter.QueueService,
                RedisQueueHelperParameter.ServiceClinet);
        }

    }
}
