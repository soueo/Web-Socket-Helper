﻿using Core.DataAccess.Model.Project.Queue;
using Core.Framework.EntityExtend;
using System;
using System.Collections.Generic;

namespace Core.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {


            using (var context = new ProjectSocketContext())
            {

                context.ProjectModuleUser.Delete(a => a.Id > 2);

                var list = new List<ProjectModuleUser>();

                for (int i = 0; i < 10; i++)
                {
                    list.Add(new ProjectModuleUser { Pass = $"123_{i}", Phone = "15252156614" });
                }

                var list2 = new List<ProjectUserGroup>();

                for (int i = 0; i < 10; i++)
                {
                    list2.Add(new ProjectUserGroup { Title = $"123_{i}" });
                }

                // 删除指定条件的数据
                context.ProjectModuleUser.Delete(a => a.Id > 2);

                // 批量修改
                context.ProjectModuleUser
                    .BulkUpdate(new ProjectModuleUser{ BaiduOpenid = "BaiduOpenid" })
                    .Where(a => a.Id == 2);

                // List to Table

                // 批量插入 Table 1
                context.ProjectModuleUser.BulkInsert(list);

                // 批量插入 Table 2
                context.ProjectUserGroup.BulkInsert(list2);

                var rows = context.BeginSaveChanges(out string error, transaction: true);
                var rows1 = context.BeginSaveChanges(out string error1);

                // 受影响行数
                Console.WriteLine(rows);

                // 错误信息
                Console.WriteLine(error);
            }

            Console.ReadKey();
        }
    }
}
