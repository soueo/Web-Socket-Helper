﻿using System;
using Core.DataAccess.Model.Projects;

namespace Core.IBusiness.IProjectModule
{
    public interface IProject
    {
        /// <summary>
        /// 验证项目信息
        /// </summary>
        /// <returns><c>true</c>, if exist was ised, <c>false</c> otherwise.</returns>
        /// <param name="userKey">用户ID</param>
        /// <param name="userToken">用户Token</param>
        /// <param name="projectToken">项目Token</param>
        Tuple<ProjectList, bool> IsExist(int userKey, string userToken, string projectToken);

        /// <summary>
        /// 获取项目信息
        /// </summary>
        /// <returns>T项目信息实体</returns>
        /// <param name="userKey">用户ID</param>
        /// <param name="projectToken">项目Token</param>
        ProjectList GetProjectInfo(int userKey, string projectToken);

    }
}
