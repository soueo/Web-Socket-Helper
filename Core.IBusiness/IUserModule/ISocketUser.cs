﻿using Core.DataAccess.Model.Project.Queue;
using System;

namespace Core.IBusiness.IUserModule
{
    public interface ISocketUser
    {

        #region 用户身份登陆

        /// <summary>
        /// 账户密码登陆
        /// </summary>
        /// <param name="user">用户名</param>
        /// <param name="password">密码</param>
        /// <param name="projectToken">项目TOKEN</param>
        /// <returns>用户临时TOKEN，和用户信息</returns>
        Tuple<ProjectModuleUser, bool> Login(string user, string password, string projectToken, int hour);

        /// <summary>
        /// 根据手机号登陆
        /// </summary>
        /// <param name="phone">手机号</param>
        /// <param name="password">密码</param>
        /// <param name="projectToken">项目TOKEN</param>
        /// <returns>用户临时TOKEN，和用户信息</returns>
        Tuple<ProjectModuleUser, bool> LoginByPhone(string phone, string password, string projectToken, int hour);

        /// <summary>
        /// 根据微信OPENID获取用户信息|注册
        /// </summary>
        /// <param name="openID">UUID|WEIXIN|ALIPAY|BAIDU</param>
        /// <param name="projectToken">项目TOKEN</param>
        /// <returns>用户临时TOKEN，和用户信息</returns>
        Tuple<ProjectModuleUser, bool> LoginByWChat(string openID, string projectToken, int hour);

        /// <summary>
        /// 根据客户端UUID登陆|注册
        /// </summary>
        /// <param name="uuid"></param>
        /// <param name="projectToken"></param>
        /// <returns></returns>
        Tuple<ProjectModuleUser, bool> LoginByClient(string uuid, string projectToken, int hour);

        /// <summary>
        /// 根据 用户token 获取 WebSocket 登陆令牌
        /// </summary>
        /// <param name="userToken">用户token</param>
        /// <param name="projectToken">项目token</param>
        /// <param name="parameter">用户参数</param>
        /// <param name="url">用户回调地址</param>
        /// <returns></returns>
        Tuple<ProjectModuleUser,string, bool> LoginWebSocketByUserToken(string userToken, string projectToken, string parameter, string url);

        /// <summary>
        /// 根据 客户端 UUID 获取 WebSocket 登陆令牌
        /// </summary>
        /// <param name="uuid"></param>
        /// <param name="projectToken">项目token</param>
        /// <param name="parameter">用户参数</param>
        /// <param name="url">用户回调地址</param>
        /// <returns></returns>
        Tuple<ProjectModuleUser, string, bool> LoginWebSocketByClient(string uuid, string projectToken, string parameter, string url);

        /// <summary>
        /// 根据微信 OPENID 获取 WebSocket 登陆令牌
        /// </summary>
        /// <param name="openID"></param>
        /// <param name="projectToken">项目token</param>
        /// <param name="parameter">用户参数</param>
        /// <param name="url">用户回调地址</param>
        /// <returns></returns>
        Tuple<ProjectModuleUser, string, bool> LoginWebSocketByWChat(string openID, string projectToken, string parameter, string url);


        #endregion

        #region 用户信息修改

        /// <summary>
        /// 修改用户信息
        /// 查找 [用户名和密码] [OPENID]
        /// 相匹配的用户
        /// </summary>
        /// <param name="model">用户信息</param>
        /// <returns>用户临时TOKEN，和用户信息</returns>
        ProjectModuleUser UpdateUserInfo(ProjectModuleUser model);

        /// <summary>
        /// 修改用户指定属性
        /// </summary>
        /// <param name="paras"></param>
        /// <returns></returns>
        ProjectModuleUser UpdateUserInfoParas(int key, string userToken, string projectToken, string paras);

        #endregion

        #region 用户身份注册

        /// <summary>
        /// 用户注册
        /// 校验 [项目是否存在] [用户名是否重复] [OPENID是否重复]
        /// </summary>
        /// <param name="model">用户信息</param>
        Tuple<bool, ProjectModuleUser, string> Reg(ProjectModuleUser model);

        #endregion

        #region 获取用户信息

        /// <summary>
        /// 根据客户端UserID获取用户信息
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="projectToken"></param>
        /// <returns></returns>
        Tuple<ProjectModuleUser, bool> LoginByKey(int userID, string projectToken, int hour);

        /// <summary>
        /// 根据临时TOKNE获取用户信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns>是否过期|用户信息</returns>
        Tuple<bool, ProjectModuleUser> GetUserInfoByToken(string token, string projectToken);

        #endregion

        #region 用户身份注销

        /// <summary>
        /// 退出登陆
        /// </summary>
        /// <param name="model"></param>
        /// <returns>是否过期|用户信息</returns>
        void OutLogin(string token, string projectToken);

        #endregion
    }
}
