﻿using log4net;
using log4net.Repository;
using System;
using System.IO;

namespace Core.Framework.Loger
{
    /// <summary>
    /// log4net
    /// </summary>
    public class LogFactory
    {
        static ILoggerRepository repository { get; set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        static LogFactory()
        {
            repository = LogManager.CreateRepository("NETCoreRepository");
            log4net.Config.XmlConfigurator.Configure(
                repository, 
                new FileInfo("log4net.config"));

        }
        /// <summary>
        /// 获取日志操作对象
        /// </summary>
        /// <param name="type">类型</param>
        /// <returns></returns>
        public static Log GetLogger(Type type)
        {
            return new Log(LogManager.GetLogger(repository.Name, type));
        }
        /// <summary>
        /// 获取日志操作对象
        /// </summary>
        /// <param name="str">名字</param>
        /// <returns></returns>
        public static Log GetLogger(string str)
        {
            return new Log(LogManager.GetLogger(repository.Name, str));
        }
    }
}
