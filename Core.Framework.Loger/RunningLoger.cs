﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

namespace Core.Framework.Loger
{
    public class RunningLoger
    {
        static string basepath = AppDomain.CurrentDomain.BaseDirectory;

        //  RunningLoger QueueMsg - List
        //  RunningLoger QueueMsg - Error
        //  RunningLoger QueueMsg - Warn
        //  RunningLoger QueueMsg - Info
        //  RunningLoger QueueMsg - DeBug
        
        static object lockList = new object();
        static object lockError = new object();
        static object lockWarn = new object();
        static object lockInfo = new object();
        static object lockDeBug = new object();  // 
        static object lockQueue = new object();  // 队列消息
        static object lockConsum = new object(); // 消费消息

        static RunningLoger()
        {
            var path1 = Path.Combine(basepath, "WebSocketsRunningLoger\\");
            if (!Directory.Exists(path1))
                Directory.CreateDirectory(path1);

            var path2 = Path.Combine(basepath, "WebSocketsRunningLoger\\Error\\");
            if (!Directory.Exists(path2))
                Directory.CreateDirectory(path2);

            var path3 = Path.Combine(basepath, "WebSocketsRunningLoger\\QueryMessage\\");
            if (!Directory.Exists(path3))
                Directory.CreateDirectory(path3);

            var path4 = Path.Combine(basepath, "WebSocketsRunningLoger\\Warn\\");
            if (!Directory.Exists(path4))
                Directory.CreateDirectory(path4);

            var path5 = Path.Combine(basepath, "WebSocketsRunningLoger\\DeBug\\");
            if (!Directory.Exists(path5))
                Directory.CreateDirectory(path5);

            var path6 = Path.Combine(basepath, "WebSocketsRunningLoger\\Info\\");
            if (!Directory.Exists(path6))
                Directory.CreateDirectory(path6);

            var path7 = Path.Combine(basepath, "WebSocketsRunningLoger\\Queue\\");
            if (!Directory.Exists(path7))
                Directory.CreateDirectory(path7);

            var path8 = Path.Combine(basepath, "WebSocketsRunningLoger\\Consum\\");
            if (!Directory.Exists(path8))
                Directory.CreateDirectory(path8);

        }

        /// <summary>
        /// 所有消息
        /// </summary>
        /// <param name="text"></param>
        public static void Add(string text)
        {
            lock (lockList)
            {
                try
                {
                    var path = Path.Combine(basepath, "WebSocketsRunningLoger\\QueryMessage\\");
                    string fileFullName
                        = Path.Combine(path, string.Format("{0}.txt", DateTime.Now.ToString("yyyy-MM-dd")));

                    File.AppendAllText(fileFullName, $"{Environment.NewLine}**** {DateTime.Now.ToString()} {Environment.NewLine}**** {text}{Environment.NewLine}**** Thread ID {Thread.CurrentThread.ManagedThreadId}{Environment.NewLine}", Encoding.UTF8);

                }
                catch
                {

                }
            }
        }

        /// <summary>
        /// 错误日志
        /// </summary>
        /// <param name="text"></param>
        public static void Error(string text)
        {
            lock (lockError)
            {
                try
                {
                    var path = Path.Combine(basepath, "WebSocketsRunningLoger\\Error\\");
                    string fileFullName
                        = Path.Combine(path, string.Format("{0}.txt", DateTime.Now.ToString("yyyy-MM-dd")));

                    File.AppendAllText(fileFullName, $"{Environment.NewLine}**** {DateTime.Now.ToString()} {Environment.NewLine}**** {text}{Environment.NewLine}**** Thread ID {Thread.CurrentThread.ManagedThreadId}{Environment.NewLine}", Encoding.UTF8);
                }
                catch
                {

                }
                
            }
        }

        /// <summary>
        /// 警告日志
        /// </summary>
        /// <param name="text"></param>
        public static void Warn(string text)
        {
            lock (lockWarn)
            {
                try
                {
                    var path = Path.Combine(basepath, "WebSocketsRunningLoger\\Warn\\");
                    string fileFullName
                        = Path.Combine(path, string.Format("{0}.txt", DateTime.Now.ToString("yyyy-MM-dd")));

                    File.AppendAllText(fileFullName, $"{Environment.NewLine}**** {DateTime.Now.ToString()} {Environment.NewLine}**** {text}{Environment.NewLine}**** Thread ID {Thread.CurrentThread.ManagedThreadId}{Environment.NewLine}", Encoding.UTF8);
                }
                catch
                {

                }
                
            }
        }

        /// <summary>
        /// Info 日志
        /// </summary>
        /// <param name="text"></param>
        public static void Info(string text)
        {
            lock (lockInfo)
            {
                try
                {
                    var path = Path.Combine(basepath, "WebSocketsRunningLoger\\Info\\");

                    string fileFullName
                        = Path.Combine(path, string.Format("{0}.txt", DateTime.Now.ToString("yyyy-MM-dd")));

                    File.AppendAllText(fileFullName, $"{Environment.NewLine}**** {DateTime.Now.ToString()} {Environment.NewLine}**** {text}{Environment.NewLine}**** Thread ID {Thread.CurrentThread.ManagedThreadId}{Environment.NewLine}", Encoding.UTF8);
                }
                catch
                {

                }
            }
        }

        /// <summary>
        /// DeBug 日志
        /// </summary>
        /// <param name="text"></param>
        public static void DeBug(string text)
        {
            lock (lockDeBug)
            {
                try
                {
                    var path = Path.Combine(basepath, "WebSocketsRunningLoger\\DeBug\\");

                    string fileFullName
                        = Path.Combine(path, string.Format("{0}.txt", DateTime.Now.ToString("yyyy-MM-dd")));

                    File.AppendAllText(fileFullName, $"{Environment.NewLine}**** {DateTime.Now.ToString()} {Environment.NewLine}**** {text}{Environment.NewLine}**** Thread ID {Thread.CurrentThread.ManagedThreadId}{Environment.NewLine}", Encoding.UTF8);
                }
                catch
                {

                }
                
            }
        }

        /// <summary>
        /// Queue 日志
        /// </summary>
        /// <param name="text"></param>
        public static void Queue(string text)
        {
            lock (lockQueue)
            {
                try
                {
                    var path = Path.Combine(basepath, "WebSocketsRunningLoger\\Queue\\");

                    string fileFullName
                        = Path.Combine(path, string.Format("{0}.txt", DateTime.Now.ToString("yyyy-MM-dd")));

                    File.AppendAllText(fileFullName, $"{Environment.NewLine}**** {DateTime.Now.ToString()} {Environment.NewLine}**** {text}{Environment.NewLine}**** Thread ID {Thread.CurrentThread.ManagedThreadId}{Environment.NewLine}", Encoding.UTF8);
                }
                catch
                {

                }
            }
        }

        /// <summary>
        /// Consum 日志
        /// </summary>
        /// <param name="text"></param>
        public static void Consum(string text)
        {
            lock (lockConsum)
            {
                try
                {
                    var path = Path.Combine(basepath, "WebSocketsRunningLoger\\Consum\\");

                    string fileFullName
                        = Path.Combine(path, string.Format("{0}.txt", DateTime.Now.ToString("yyyy-MM-dd")));

                    File.AppendAllText(fileFullName, $"{Environment.NewLine}**** {DateTime.Now.ToString()} {Environment.NewLine}**** {text}{Environment.NewLine}**** Thread ID {Thread.CurrentThread.ManagedThreadId}{Environment.NewLine}", Encoding.UTF8);
                }
                catch
                {

                }
            }
        }


    }
}
