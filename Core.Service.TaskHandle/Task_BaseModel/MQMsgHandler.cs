﻿namespace Core.Service.TaskHandle
{
    public class MQMsgHandler
    {
        /// <summary>
        /// 主题
        /// </summary>
        public string Template { get; set; }

        /// <summary>
        /// 任务事件
        /// </summary>
        public IMQMsgListener Listener { get; set; }
    }
}
