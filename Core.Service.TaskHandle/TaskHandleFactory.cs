﻿using Core.Framework.Loger;
using Core.Framework.Model.WebSockets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Service.TaskHandle
{
    /// <summary>
    /// 任务事件工厂
    /// </summary>
    public class TaskHandleFactory
    {
        /// <summary>
        /// 消息监听器
        /// </summary>
        public static List<MQMsgHandler> MQMsgHandlers;

        static bool Inited = false;

        /// <summary>
        /// 初始化消息监听器
        /// </summary>
        /// <param name="init">是否强制初始化</param>
        /// <returns></returns>
        public static bool Initialization(bool init = false)
        {
            if (!init && Inited)
                return true;

            MQMsgHandlers = new List<MQMsgHandler>();

            try
            {

                var types = AppDomain.CurrentDomain.GetAssemblies()
                    .SelectMany(a => a.GetTypes().Where(t => t.GetInterfaces().Contains(typeof(IMQMsgListener))))
                    .ToArray();

                foreach (var aHandler in types)
                {
                    var atts = aHandler.CustomAttributes;
                    if (atts?.Count() > 0)
                    {
                        foreach (var att in atts)
                        {
                            if (att.AttributeType == typeof(ListennerAttribute))
                            {
                                MQMsgHandler aMQMsgHandler = new MQMsgHandler
                                {
                                    Template = att.NamedArguments[0].TypedValue.Value.ToString(),
                                    Listener = (IMQMsgListener)Activator.CreateInstance(aHandler)
                                };
                                MQMsgHandlers.Add(aMQMsgHandler);
                            }
                        }
                    }
                }

                Inited = true;

                return Inited;
            }
            catch (Exception ex)
            {
                LogFactory.GetLogger(typeof(TaskHandleFactory)).Debug(ex);
                MQMsgHandlers = null;
                return false;
            }
        }

        /// <summary>
        /// 执行函数
        /// </summary>
        /// <param name="DoWordIn"></param>
        public static void DoDispatch(
            MessageQueue message, 
            Func<MQMsgHandler, bool> expression,
            Action<string, string> callBack // 异常 / template / info
            )
        {
            try
            {
                if (Initialization() == false)
                    throw new Exception();

                var result = false;

                foreach (var aHandler in MQMsgHandlers)
                {
                    if (expression.Invoke(aHandler))
                    {
                        try
                        {
                            var DoWorkOut = aHandler.Listener.DoWork(message);

                            if (DoWorkOut.OK == false)
                                aHandler.Listener.Errer(DoWorkOut);
                            else
                                aHandler.Listener.Success(DoWorkOut);

                            result = true;
                        }
                        catch (Exception ex)
                        {
                            callBack(aHandler.Template, ex.Message);
                            LogFactory.GetLogger(typeof(TaskHandleFactory)).Debug(ex);
                        }
                    }
                }

                if (!result)
                    callBack(null, "未找执行");

            }
            catch (Exception ex)
            {
                LogFactory.GetLogger(typeof(TaskHandleFactory)).Debug(ex);
            }
        }

    }
}
