﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Core.DataAccess.Model.Projects
{
    public partial class ProjectUser
    {
        public int Id { get; set; }
        [MaxLength(11, ErrorMessage = "请输入正确的手机号码")]
        [MinLength(11, ErrorMessage = "请输入正确的手机号码")]
        public string Phone { get; set; }
        public string UserName { get; set; }
        public string Pass { get; set; }
        public string Token { get; set; }
        public DateTime? EndTime { get; set; }
        public DateTime? AddTime { get; set; }
    }
}
