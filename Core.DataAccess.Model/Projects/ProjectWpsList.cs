﻿using System;
using System.Collections.Generic;

namespace Core.DataAccess.Model.Projects
{
    public partial class ProjectWpsList
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string RequestHead { get; set; }
        public string RequestPath { get; set; }
        public string RequestType { get; set; }
        public string RetExs { get; set; }
        public string SendExs { get; set; }
        public DateTime? AddTime { get; set; }
        public int? WpsKey { get; set; }
        public bool? State { get; set; }
    }
}
