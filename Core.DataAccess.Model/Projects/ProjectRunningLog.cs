﻿using System;
using System.Collections.Generic;

namespace Core.DataAccess.Model.Projects
{
    public partial class ProjectRunningLog
    {
        public int Id { get; set; }
        public string UserToken { get; set; }
        public string UserPhone { get; set; }
        public string ProjectToken { get; set; }
        public int? Type { get; set; }
        public string Content { get; set; }
        public DateTime? AddTime { get; set; }
    }
}
