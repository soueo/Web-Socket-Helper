﻿using System;
using System.Collections.Generic;

namespace Core.DataAccess.Model.Projects
{
    public partial class ProjectList
    {
        public int Id { get; set; }
        public int UserKey { get; set; }
        public string Title { get; set; }
        public string Info { get; set; }
        public bool Ios { get; set; }
        public bool Android { get; set; }
        public bool Pc { get; set; }
        public bool Wap { get; set; }
        public bool Weixin { get; set; }
        public string Token { get; set; }
        public DateTime? AddTime { get; set; }
    }
}
