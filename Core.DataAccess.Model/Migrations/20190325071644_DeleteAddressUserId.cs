﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.DataAccess.Model.Migrations
{
    public partial class DeleteAddressUserId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Project_Module_Address");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Project_Module_Address",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    addTime = table.Column<DateTime>(type: "datetime", nullable: true),
                    addressName = table.Column<string>(unicode: false, maxLength: 30, nullable: true),
                    city = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    def = table.Column<bool>(nullable: true, defaultValueSql: "((0))"),
                    district = table.Column<string>(unicode: false, maxLength: 30, nullable: true),
                    info = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    projectToken = table.Column<string>(unicode: false, maxLength: 21, nullable: false),
                    province = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    tag = table.Column<string>(unicode: false, maxLength: 4, nullable: true),
                    userId = table.Column<string>(nullable: true),
                    userName = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    userPhone = table.Column<string>(unicode: false, maxLength: 11, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Project_Module_Address", x => x.id);
                });
        }
    }
}
