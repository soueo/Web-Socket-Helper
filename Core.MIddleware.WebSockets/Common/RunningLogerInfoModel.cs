﻿using Core.Framework.Model.WebSockets;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Middleware.WebSockets
{
    public class RunningLogerInfoModel
    {
        /// <summary>
        /// 消息列表
        /// </summary>
        public QueryMessage Message { get; set; }

        /// <summary>
        /// 客户端
        /// </summary>
        public List<string> clients { get; set; } = new List<string>();

        /// <summary>
        /// 是否发布
        /// </summary>
        public bool IsPublish { get; set; } = false;
    }
}
