﻿using Core.DataAccess.Model;
using Core.DataAccess.Model.Project.Queue;
using Core.Framework.Util.Attributes;
using Core.IBusiness.ISocketModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Core.Business.SocketModel
{
    public class SocketRelationship : ISocketRelationship
    {
        public Tuple<ProjectUserGroup, string, bool> CreateGroup(ProjectUserGroup model)
        {
            using (var context = new ProjectSocketContext())
            {

                var tuple = model.IsValid(Attributes.IsValidEnum.reg);

                if (!tuple.Item1)
                    return new Tuple<ProjectUserGroup, string, bool>(model, tuple.Item2, false);

                context.ProjectUserGroup.Add(model);
                context.SaveChanges();

                return new Tuple<ProjectUserGroup, string, bool>(model, "添加会员成功", true);
            }

            ;
        }

        public Tuple<ProjectUserGroupMember, string, bool> CreateGroupMember(ProjectUserGroupMember model)
        {
            using (var context = new ProjectSocketContext())
            {

                var tuple = model.IsValid(Attributes.IsValidEnum.reg);

                if (!tuple.Item1)
                    return new Tuple<ProjectUserGroupMember, string, bool>(model, tuple.Item2, false);

                context.ProjectUserGroupMember.Add(model);
                context.SaveChanges();

                return new Tuple<ProjectUserGroupMember, string, bool>(model, "添加会员成功", true);
            };
        }

        public Tuple<ProjectUserGroup, bool> DeleteGroup(int id, string projectToken)
        {
            using (var context = new ProjectSocketContext())
            {

                var model = context
                    .ProjectUserGroup
                    .Where(a => a.Id == id && a.ProjectToken == projectToken)
                    .FirstOrDefault();
                if (model?.Id > 0)
                {
                    context.ProjectUserGroup.Remove(model);
                    context.SaveChanges();
                    return new Tuple<ProjectUserGroup, bool>(model, true);
                }

                return new Tuple<ProjectUserGroup, bool>(model, false);

            }

            ;
        }

        public Tuple<ProjectUserGroupMember, bool> DeleteGroupMember(int id, string projectToken)
        {
            using (var context = new ProjectSocketContext())
            {

                var model = context
                    .ProjectUserGroupMember
                    .Where(a => a.Id == id && a.ProjectToken == projectToken)
                    .FirstOrDefault();
                if (model?.Id > 0)
                {
                    context.ProjectUserGroupMember.Remove(model);
                    context.SaveChanges();
                    return new Tuple<ProjectUserGroupMember, bool>(model, true);
                }

                return new Tuple<ProjectUserGroupMember, bool>(model, false);

            }

            ;
        }

        public Tuple<ProjectUserGroup, string, bool> UpdateGroup(ProjectUserGroup model)
        {
            using (var context = new ProjectSocketContext())
            {

                var tuple = model.IsValid(Attributes.IsValidEnum.update);

                if (!tuple.Item1)
                    return new Tuple<ProjectUserGroup, string, bool>(model, tuple.Item2, false);

                context.ProjectUserGroup.Update(model);
                context.SaveChanges();

                return new Tuple<ProjectUserGroup, string, bool>(model, "修改成功", true);
            }

            ;
        }

        public Tuple<ProjectUserGroupMember, string, bool> UpdateGroupMember(ProjectUserGroupMember model)
        {
            using (var context = new ProjectSocketContext())
            {

                var tuple = model.IsValid(Attributes.IsValidEnum.update);

                if (!tuple.Item1)
                    return new Tuple<ProjectUserGroupMember, string, bool>(model, tuple.Item2, false);

                context.ProjectUserGroupMember.Update(model);
                context.SaveChanges();

                return new Tuple<ProjectUserGroupMember, string, bool>(model, "修改成功", true);
            }

            ;
        }

        public Tuple<List<ProjectUserGroup>, List<ProjectUserGroupMember>, bool> GetAll(string userKey,
            string projectToken)
        {
            using (var context = new ProjectSocketContext())
            {

                var listUserGroup = context
                    .ProjectUserGroup
                    .Where(a => a.UserId == userKey && a.ProjectToken == projectToken)
                    .ToList();

                var listUserGroupMember = context
                    .ProjectUserGroupMember
                    .Where(a => a.UserId == userKey && a.ProjectToken == projectToken)
                    .ToList();

                return new
                    Tuple<List<ProjectUserGroup>, List<ProjectUserGroupMember>, bool>(listUserGroup,
                        listUserGroupMember, true);
            }

            ;
        }

        public Tuple<List<ProjectUserGroupMember>, bool> GetGroupMembersByGroupKey(int groupKey, string projectToken)
        {
            using (var context = new ProjectSocketContext())
            {

                var list = context
                    .ProjectUserGroupMember
                    .Where(a => a.GroupKey == groupKey && a.ProjectToken == projectToken)
                    .ToList();

                return new Tuple<List<ProjectUserGroupMember>, bool>(list, true);
            }

            ;
        }

        public Tuple<ProjectUserGroupMember, bool> GetItemGroupMember(int id, string projectToken)
        {
            using (var context = new ProjectSocketContext())
            {

                var model = context
                    .ProjectUserGroupMember
                    .Where(a => a.Id == id && a.ProjectToken == projectToken)
                    .FirstOrDefault();

                return new Tuple<ProjectUserGroupMember, bool>(model, true);
            }

            ;
        }

    }

}
